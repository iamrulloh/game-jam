extends Area2D

func _on_AreaTrigger_body_entered(body):
	if body.is_in_group("Shots"):
		body.get_node("CollisionShape2D").disabled = true
		body.get_node("Sprite").play("Explode")
	
	if body.is_in_group("Items"):
		body.queue_free()
	