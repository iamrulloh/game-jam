extends Control

func _ready():
	get_node("MarginContainer/VBoxContainer/VBoxContainer/Label2").set_text("Score: "+str(ceil(global.score)))

func _on_home_pressed():
	get_tree().change_scene(str("res://Scenes/MainMenu.tscn"))


func _on_restart_pressed():
	global.score = 0
	global.gravity_scale = 1
	global.delay = 2
	global.ms = 0
	global.s = 0
	global.m = 0
	get_tree().paused = false
	get_tree().change_scene(str("res://Scenes/World.tscn"))
