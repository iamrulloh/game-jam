extends Node2D

var shot = preload("res://Scenes/Shot.tscn")
var item = preload("res://Scenes/Item.tscn")
var once = true

onready var width = get_viewport().size.x
onready var shot_timer = Timer.new()
onready var item_timer = Timer.new()

func _ready():
	shot_timer.set_wait_time(global.delay)
	shot_timer.connect("timeout", self, "on_shot_timeout_complete")
	item_timer.set_wait_time(10)
	item_timer.connect("timeout", self, "on_item_timeout_complete")
	add_child(shot_timer)
	add_child(item_timer)
	create_shot_instance()
	shot_timer.start()
	item_timer.start()
	
func _process(delta):
	if global.m == 0:
		if global.s > 0 and global.s % 20 == 0 and once == true:
			global.gravity_scale += 5
			if global.delay > 1:
				global.delay -= 1
			else:
				global.delay = global.delay / 2.0
			shot_timer.set_wait_time(global.delay)
			once = false
		elif global.s > 0 and global.s % 20 != 0:
			once = true
	else:
		if global.s % 20 == 0 and once == true:
			global.gravity_scale += 5
			if global.delay > 1:
				global.delay -= 1
			else:
				global.delay = global.delay / 2.0
			shot_timer.set_wait_time(global.delay)
			once = false
		elif global.s % 20 != 0:
			once = true
	
func on_shot_timeout_complete():
	create_shot_instance()
	
func on_item_timeout_complete():
	create_timer_instance()
	
func create_shot_instance():
	randomize()
	var new_shot = shot.instance()
	new_shot.set_position(Vector2(rand_range(0, width), 0))
	new_shot.set_gravity_scale(global.gravity_scale)
	add_child(new_shot)
	
func create_timer_instance():
	randomize()
	var new_item = item.instance()
	new_item.set_position(Vector2(rand_range(0, width), 0))
	new_item.set_gravity_scale(global.gravity_scale)
	add_child(new_item)
	randomize()
	item_timer.set_wait_time(rand_range(10, 30))
