extends Node2D

func _on_play_pressed():
	global.score = 0
	global.gravity_scale = 1
	global.delay = 2
	global.ms = 0
	global.s = 0
	global.m = 0
	get_tree().paused = false
	get_tree().change_scene(str("res://Scenes/World.tscn"))
	
func _on_info_pressed():
	get_node("InfoScreen").visible = true
	
func _on_exit_pressed():
	get_tree().quit()

func _on_home_pressed():
	get_node("InfoScreen").visible = false
