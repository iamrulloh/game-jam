extends KinematicBody2D

export (int) var Gravity = 1200

export (int) var speed = 400

const UP = Vector2(0,-1)

var velocity = Vector2()
var shield = false

var shield_timer = null
var game_over = preload("res://Scenes/GameOver.tscn")

onready var sprite = get_node("Sprite")
onready var shield_area = get_node("Shield/ShieldCollision")

func get_input():
	var left_key = Input.is_action_pressed('ui_left')
	var right_key = Input.is_action_pressed('ui_right')
	velocity.x = 0
	if right_key:
		velocity.x += speed
	if left_key:
		velocity.x -= speed

func _physics_process(delta):
	velocity.y += delta * Gravity
	get_input()
	velocity = move_and_slide(velocity, UP)
	
func _process(delta):
	if !shield:
		if velocity.x != 0:
			sprite.play("Walk")
			if velocity.x > 0:
				sprite.set_flip_h(false)
			else:
				sprite.set_flip_h(true)
		else:
			sprite.play("Idle")
	else:
		if velocity.x != 0:
			sprite.play("WShield")
			if velocity.x > 0:
				sprite.set_flip_h(false)
			else:
				sprite.set_flip_h(true)
		else:
			sprite.play("Shield")

func obtain(body):
	if body.is_in_group("Shots") and shield_area.disabled:
		body.get_node("Sprite").play("Explode")
		sprite.play("Death")
		global.score = (global.m*1000)+(global.s*16.9491525424)+(global.ms*0.2872737719)
		
		var canvas_layer = get_tree().get_root().get_node("World").get_node("CanvasLayer")
		var new_game_over = game_over.instance()
		canvas_layer.add_child(new_game_over)
		get_tree().paused = true
	
	if !shield_area.disabled:
		shield_active(body)
		
	if body.is_in_group("Items"):
		body.queue_free()
		
#		var skill = randi() % 2
		var skill = 0
		if skill == 0:
			shield_timer = Timer.new()
			shield_timer.connect("timeout", self, "on_shield_timer_timeout")
			add_child(shield_timer)
			shield_timer.set_wait_time(5)
			shield = true
			shield_area.disabled = false
			shield_timer.start()
		elif skill == 1:
			global.gravity_scale += 5
			global.delay /= 2.0
		else:
			global.gravity_scale = 1
			global.delay = 2

func shield_active(body):
	if body.is_in_group("Shots"):
		body.get_node("Sprite").play("Explode")
		
func on_shield_timer_timeout():
	shield = false
	shield_area.disabled = true
	shield_timer.queue_free()
