extends RichTextLabel

func _process(delta):
	if global.ms > 59:
		global.s +=1
		global.ms = 0
	if global.s > 59:
		global.m += 1
		global.s = 0
		
	set_text(str(global.m)+":"+str(global.s)+":"+str(global.ms))

func _on_ms_timeout():
	global.ms += 1
