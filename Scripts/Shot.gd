extends RigidBody2D

var sprite

func _ready():
	add_to_group("Shots")
	sprite = get_node("Sprite")
	sprite.play('Shot')

func play_next():
	if sprite.get_animation() == "Explode":
		queue_free()
	